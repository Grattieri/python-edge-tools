/*
 *
 * Demo Javascript CamBuilder
 * Version 1.0 - August 2019
 * By Massimo Grattieri
 * mail: massimo.grattieri@boschrexroth.com
 *
 * Based on boschrexroth.webconnectorclient.min.js
 *
 */


$(document).ready(function () {
	console.log("Ready"); 
	var builder = new camBuilder();
});




class camBuilder  {
	constructor() {
		this.debug=false; 
		this.canvas = document.getElementById("graph"); 
		this.ctx = this.canvas.getContext("2d");
		this.points=[];
		this.cam=[];
		this.scaleX = 360/(503 - 45) ; 
		this.scaleY = 400/-(141) ;
		this.OffsetX = -45; 
		this.OffsetY = -162;
		this.MouseOffsetX = -15; 
		this.MouseOffsetY = -20 ; 
		this.drag= 0 ;
		this.mode = 1; // -1:del  0:move  1:add 
		this.selectedPoint= -1  ; 
		this.camTableView;
		this.camPointsView;
		setInterval( this.timer, 1000);
		this.init(); 
		if (this.debug) {( console.log(this) ) } ; 
	}
	timer() {$('#time').html(Date()); }
	init(){
		// ---- Preset radio button and add click events --- //
		var scope=this; 
		scope.updateMode(1);
		$('#addMode').click( function() { scope.updateMode(1);});
		$('#delMode').click( function() { scope.updateMode(-1);});
		$('#moveMode').click( function() { scope.updateMode(0);});
		scope.canvas.addEventListener("mousedown",this.mousedown.bind(this) );

		// ---- Preset initial points ------- // 
		scope.points.push({step: 0,x:   0.0, y:   0.0 , v: 0.0 , a : 0.0, hub : 200.0 });
		scope.points.push({step: 1,x: 360.0, y: 300.0 , v: 0.0 , a : 0.0, hub : 200.0 });

		// --- vue points table 
		scope.tablePointsView = new Vue({ el: '#tableOfpoints',	data: {	rows: JSON.parse(JSON.stringify(scope.points))}, 
			methods: {	
				update: function (event) { scope.updateData();}
			}
		}); 

		// --- vue cam table 
		scope.camTableView = new Vue({ el: '#camTable',	data: {	rows: JSON.parse(JSON.stringify(scope.cam))}}); 
	
		scope.updateData() ;
	}
	
	updateData() {
		console.log("update data on table "); 
		console.log(this.points ); 
		this.points=[];
		for(var i in this.tablePointsView.rows ){
			var point = this.tablePointsView.rows[i]; 
			this.points.push({step:point.step, x:parseFloat(point.x),  y:parseFloat(point.y) , v:parseFloat(point.v) , a:parseFloat(point.a)    , hub:parseFloat(point.hub)   });
		}
		console.log(this.points ); 
		this.updateTable(); 
	}
	
	updateTable(){
		console.log("update data on graph "); 
		console.log(this.points ); 
		this.sortPoints();
		console.log(this.points ); 
		this.tablePointsView.rows = JSON.parse(JSON.stringify(this.points)); 
		$("#hub").val("").attr("placeholder",this.points[0].hub);
		this.drawpoints();
		this.generateProfile();
		this.ctx.stroke();
		
	}
	
	updateMode(mode) {
		var a=false,d=false,m=false;
		this.mode = mode ; 
		if (this.mode==-1) {	d=true;}
		if (this.mode== 1) {	a=true;}
		if (this.mode== 0) {	m=true;}
		$('#addMode').prop("checked", a );
		$('#delMode').prop("checked", d );
		$('#moveMode').prop("checked", m );
		console.log('Add ='+a,'Del='+d,'Move='+m);
	}
	sortPoints(){
		this.points.sort(function(a, b) {
			return ((a.x < b.x) ? -1 : ((a.x == b.x) ? 0 : 1));
		});
		
		for(var i=0;i< (this.points.length);i+=1) {
			this.points[i].step=i; 
		}
		
	}
	drawpoints(){
		this.ctx.beginPath();
		this.ctx.clearRect(0, 0, 512, 300);
		for(var i=0;i< (this.points.length);i+=1) {
			var unitX = this.points[i].x;
			var unitY = this.points[i].y;
			var drawX = (unitX / this.scaleX - this.OffsetX ) + this.MouseOffsetX;  
			var drawY = (unitY / this.scaleY - this.OffsetY ) + this.MouseOffsetY;  
			if ( i  == this.selectedPoint ) {
				this.ctx.fillStyle = "Red";
				this.ctx.fillRect(drawX - 4, drawY - 4, 8, 8);
			}else{	
				this.ctx.fillStyle = "Black";
				this.ctx.fillRect(drawX - 3, drawY - 3, 6, 6);
			}		
			this.ctx.strokeText(i, drawX, drawY - 6);  
			if (this.debug) { console.log(i, unitX , unitY ) }; 
		}

	}
	lookForPoint( x ) {
		for(var i=0;i< (this.points.length);i+=1) {
			if ( this.points[i].x > x ) { return i-1;  }
		} return -1; 
	}
	poli5( x, x0, y0, v0, a0, x1, y1, v1, a1 ) {
		var T  = x1 - x0;
    	var h  = y1 - y0;
    	var c0 = y0;
    	var c1 = v0;
    	var c2 = 1.0/2.0 * a0;
    	var c3 = 1.0/(2.0*Math.pow(T,3)) * (  20.0 * h  - ( 8.0 * v1 + 12.0 * v0 ) * T - ( 3.0 * a0 - a1       ) * T * T  );
    	var c4 = 1.0/(2.0*Math.pow(T,4)) * ( -30.0 * h  + (14.0 * v1 + 16.0 * v0 ) * T + ( 3.0 * a0 - 2.0 * a1 ) * T * T  );
    	var c5 = 1.0/(2.0*Math.pow(T,5)) * (  12.0 * h  -  6.0 * (v1 + v0 )        * T + ( a1 - a0 ) * T * T  );
		var y = c0 + c1 * (x-x0) + c2 * (Math.pow((x-x0),2)) + c3 * (Math.pow((x-x0),3)) + c4 * (Math.pow((x-x0),4)) + c5 * (Math.pow((x-x0),5));
		return y
	} 
	generateProfile(){
		this.cam=[];
		var hub = parseFloat(this.points[0].hub );
		console.log("Generate profile and cam with Hub =" + hub ); 
		var drawX = (this.points[0].x / this.scaleX - this.OffsetX ) + this.MouseOffsetX;  
		var drawY = (this.points[0].y / this.scaleY - this.OffsetY ) + this.MouseOffsetY;  
		var x, y, p0, p1;
		this.ctx.strokeStyle = "Blue";
		this.ctx.moveTo(drawX, drawY);
		for(var n=0;n<1024.0;n+=1) {
			x = n / 1024.0 * 360.0; 
			p0 = this.lookForPoint(x);
			p1 = p0 + 1 ;
			y = this.poli5(x, this.points[p0].x, this.points[p0].y,this.points[p0].v, 0, this.points[p1].x, this.points[p1].y,this.points[p1].v, 0);    
			drawX = (x / this.scaleX - this.OffsetX ) + this.MouseOffsetX;  
			drawY = (y / this.scaleY - this.OffsetY ) + this.MouseOffsetY;  
			this.ctx.lineTo(drawX, drawY);
			y = y / hub * 100.0; 
			this.cam.push({ 'step': p1 ,'point': n , 'value': Number((y).toFixed(8))});
		} 
			this.camTableView.rows= JSON.parse(JSON.stringify(this.cam)); 

	}

	onPoint( x ,y ) {
		for(var i=0;i< (this.points.length);i+=1) {
			if ( ( Math.abs( x-this.points[i].x ) < 15 )  && ( Math.abs( y-this.points[i].y ) < 15 ) ) { return i;  }
		} return -1; 
	}
	mousedown(e){
		console.log("mouse down") ;
		if (this.drag ) {
			this.canvas.removeEventListener("mousemove",this.mousemove); 	//stop dragging
			this.drag = 0; 	
			this.selectedPoint = -1; 
		}else{
			var x = e.layerX ;
			var y = e.layerY ;
			var unitX = ( x + this.OffsetX ) * this.scaleX ;
			var unitY = ( y + this.OffsetY ) * this.scaleY ;
			var drawX = (unitX / this.scaleX - this.OffsetX ) + this.MouseOffsetX;  
			var drawY = (unitY / this.scaleY - this.OffsetY ) + this.MouseOffsetY;  
			if (this.debug){
				console.log("mouse down \n reading " +  x +"/" + y + "\n values " + unitX + "/" + unitY + "\n draw " + drawX + "/" + drawY ) ;
				console.log(this.OffsetX, this.scaleX ) ;
				console.log(this.OffsetY, this.scaleY ) ;
			}
			this.selectedPoint = this.onPoint(unitX, unitY ); 
			console.log("Selected point =" + this.selectedPoint) ;
			if ((this.selectedPoint == -1) && (this.mode==1)) {				// Add a point	
				console.log("add a point"); 
				this.points.push({'step':this.points.length ,'x': Number((unitX).toFixed(3)), 'y': Number((unitY).toFixed(3)) , 'v': 0.0 , 'a' : 0.0});

			} else if ((this.selectedPoint > -1) && (this.mode == -1 )) {			// Remove selected point 
				console.log("delete a point"); 
				this.points.splice(this.selectedPoint,1); 
				this.selectedPoint = -1;

			} else if ((this.selectedPoint > -1) && (this.mode == 0 )) {			// Move selected point 
				console.log("move a point");  
				this.drag = 1 ; 
				this.canvas.addEventListener("mousemove",this.mousemove.bind(this)); //start dragging
		
			} else {																// Do nothing  
				console.log("action unmanaged"); 
			} 
		}
		this.updateTable();
		if (this.debug) {( console.log(this) ) } ; 
	}

	mousemove(e) {
		var x = e.layerX ;
		var y = e.layerY ;
		var unitX = ( x + this.OffsetX ) * this.scaleX ;
		var	unitY = ( y + this.OffsetY ) * this.scaleY ;
		var drawX = (unitX / this.scaleX - this.OffsetX ) + this.MouseOffsetX;  
		var drawY = (unitY / this.scaleY - this.OffsetY ) + this.MouseOffsetY;

	if ((this.selectedPoint > 0) && (this.selectedPoint < this.points.length-1)){
		this.points[this.selectedPoint].y = unitY; 
		if ((unitX >= this.points[this.selectedPoint-1].x) && (unitX <= this.points[this.selectedPoint+1].x)) {
			this.points[this.selectedPoint].x = unitX; 
		}  
	} else if (this.selectedPoint == 0) {
		this.points[this.selectedPoint].x = 0.0; 
		this.points[this.selectedPoint].y = unitY; 
	} else if (this.selectedPoint == this.points.length-1) {
		this.points[this.selectedPoint].x = this.points[this.points.length-1].x ; 
		this.points[this.selectedPoint].y = unitY; 
	} else {
		//
	}
	this.updateTable();
}
}

