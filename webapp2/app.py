#!/usr/bin/env python3
########################################################################
# Demo code : WEBAPP2
########################################################################
import time, datetime, json, threading, os, sys
from flask import Flask
from flask import send_from_directory
import paho.mqtt.client as mqtt
import lib
########################################################################

broker_address="localhost"
dir_path = os.path.dirname(os.path.realpath(__file__))
web_path = os.path.dirname(os.path.realpath(__file__))+'/WEBROOT/'
print("Current application path :" + dir_path )
print("Current webroot path :" + web_path )

##############################################################
#print(lib.fft(samples=[0,0,0,1]))
print(lib.fft(img_path=web_path))
print(lib.rms())


data={}
data['dummy']='WEBAPP2'

################# MQTT ########################
def on_message(client, userdata, message):
	topic = str(message.topic)
	payload = str(message.payload.decode("utf-8"))
	data[topic] = payload

client = mqtt.Client("P1")
client.on_message=on_message
print("connecting to broker")
client.connect(broker_address)
client.loop_start()
client.subscribe("edge/data/#")
################ SERVLET ########################
app = Flask(__name__  )

@app.route('/<path:path>')
def static_file(path):
	return send_from_directory(web_path,path)

@app.route("/data")
def data_res():
	def _round(v):
		return round(v,2) if isinstance(v, float) else v

	return (json.dumps({k:_round(v) for k, v in data.items() } ))

@app.route("/")
def home():
	return send_from_directory(web_path,'index.html')

t1 = threading.Thread(target=app.run , args=('0.0.0.0', 5000, None, True )).start()

##################################################

while 1 :

	time.sleep(1)
