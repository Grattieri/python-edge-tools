# edge-tools

Per installare mqtt-edge :

    sudo snap install  mqtt-edge_2.1_amd64.snap --devmode

Per eseguire la snap dimostrativa mqtt-egde :

    mqtt-edge

Verrà eseguito il codice dimostrativo dello script python :

    mqtt_edge_client.py 

Fare il build della snap, per esempio dopo aver modificato il codice python:

    a - clonare il codice e modificarlo
    b - Preparare enviroment per usare snapcraft ( strumento standard per fare il build )
    c - Lanciare snapcraft nello stesso folder di snapcraft.yalm
    
    
a- Clonare il codice 
    
    1 - installare git 
        sudo apt update
        sudo apt install git
        
    2 - clonare repositoty
        git clone https://gitlab.com/Grattieri/edge-tools.git
    
    3 - editare lo script python apportando le proprie modifiche
        nano mqtt_edge_client.py
        
b1- Preparare enviroment per usare snapcraft ( Ubuntu 18.04 - standard ) : 

    sudo apt update
    sudo apt install build-essential
    sudo snap install --classic snapcraft 
    sudo snap install --beta multipass --classic 

b2- Preparare enviroment per usare snapcraft (  Soluzione con Docker ) : 

    scaricare immagine docker core/snapcraft standard :
        docker pull snapcore/snapcraft:stable 
    lanciare immagine:
        docker -it snapcore/snapcraft --name mysnapcraft /bin/bash 
    entrare nel container ed installare: 
        docker attach mysnapcraft 
        apt-get update
        apt-get dist-upgrade
        apt-get install python3
        apt-get install python3-pip
        apt-get install python-distutils-extra

c1- Lanciare snapcraft nel folder di snapcraft.yalm ( Ubuntu 18.04 - standard ) :

    snapcraft clean   ( ripulisce : non indispensabile)
    snapcraft prime   ( per verificare prima di generare lo snap : non insispensabile )
    snapcraft         ( produce lo snap )
    unsquashfs -l *.snap ( per spacchettare lo snap appena creato e vedere che ci sia tutto )

c2- Lanciare snapcraft nel folder di snapcraft.yalm ( Soluzione con Docker) :

    come sopra ( c1 )  se ci si trova nel container.
    Mentre dalla macchina host  :
    docker run -v "$PWD":/build -w /build snapcore/snapcraft:stable snapcraft
    unsquashfs -l *.snap ( per spacchettare lo snap appena creato e vedere che ci sia tutto )
    risistemare i privilegi dei folder ( altrimenti la prossima volta chiede diritti di ammistratore ) :
    sudo chown --changes --recursive $USER:$USER .  
    
Documentazione : 

    Dettagli sulla struttura di snapcraft.yalm :
    https://snapcraft.io/docs/snapcraft-yaml-reference

    Beginner tutorial :
    https://www.youtube.com/watch?v=BEp_l2oUcD8
    
    Build on docker :
    https://snapcraft.io/docs/build-on-docker
    
TODO :

    enviroment : 
        testare container su rasperryPi  
        testare container su PR21

    code:
        autostart
        condivisione folder
        InfluxDB-Python
        Inserire mosquitto 
        
    
    
        
        
    

    
    
