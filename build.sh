#!/bin/bash

echo "----- CHANGE DIRECTORY INSIDE CONTAINER --------"
sleep 2
cd /build
ls
echo "----  VERSION ------"
sleep 2
snapcraft  --version
echo "----  CLEAN ------"
sleep 2
snapcraft clean
echo "----  PRIME  ------"
sleep 2
snapcraft prime
echo "----  BUILD ------"
sleep 2
snapcraft
echo "----- ls *.snap  --------"
sleep 2
ls *.snap
#echo "----- unsquashfs -l *.snap  --------"
#sleep 2
#unsquashfs -l *.snap


