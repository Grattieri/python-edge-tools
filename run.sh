#!/bin/bash

path="/home/$(ls /home)"

echo $path

if [ -f $path/python-code/.webapp/app.py ]; then
	python3 $path/python-code/.webapp/app.py
	exit 0
fi

if [ -f $path/python-code/app.py ]; then
	python3 $path/python-code/app.py
	exit 0
fi

if [ -d $path ]; then
	mkdir $path/python-code
	cp $SNAP/bin/app.py $path/python-code/app.py
	python3 $path/python-code/app.py
	exit 0
fi

#python3 $SNAP/bin/app.py

exit 1


