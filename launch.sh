#!/bin/bash

#read -p "Press any key to continue... " -n1 -s
sudo chown --changes --recursive massimo:massimo .
sudo snap remove python-edge-tools
snapcraft clean
sudo rm *.snap
sudo rm -rf snap
sudo rm -rf prime
sudo rm -rf stage
sudo rm -rf parts
echo "---- HOST, set correct user to this folder ---- "
#read -p "Press any key to continue... " -n1 -s
ls -la

echo "--- RUN CONTAINER ----"
#read -p "Press any key to continue... " -n1 -s
docker run --rm -v "$PWD":/build --name snapcraft massimog/snapcraft bin/bash -c "/build/build.sh"
docker ps -a

echo "---- HOST, set correct user to this folder ---- "
read -p "Press any key to continue... " -n1 -s
sudo chown --changes --recursive massimo:massimo .
#read -p "Press any key to continue... " -n1 -s
ls *.snap
#echo "---- Copy snap to /var/www/html/data/ ----"
#cp *.snap /var/www/html/data

echo "---- Install snap  ----"
#read -p "Press any key to continue... " -n1 -s
sudo snap install python-edge-tools_1.0_amd64.snap --devmode
sudo snap logs python-edge-tools
#read -p "Press any key to continue... " -n1 -s

