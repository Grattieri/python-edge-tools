#!/usr/bin/env python3
########################################################################
########################################################################
import numpy as np
import matplotlib.pyplot as plotter
import time
########################################################################


def init_json_data() :
	data={}
	data['timestamp'] = time.time()
	for i in ('counter','operative','maintenance','fault'):
        	data[i+'_timestamp'] = time.time()
        	data[i+'_ever'] = 0
        	data[i+'_year'] = 0
        	data[i+'_month'] = 0
        	data[i+'_hour'] = 0
	return data




def rms(samples=[] ):
	#Simulation
	t = np.arange( 0.0 , 10.0, 1 / 100);
	if len(samples)==0 :
		signal1Frequency     = 4;
		signal2Frequency     = 7;
		amplitude1 = np.sin(2*np.pi*signal1Frequency*t)
		amplitude2 = np.sin(2*np.pi*signal2Frequency*t)
		samples = amplitude1 +  amplitude2
	else:
		samples = np.array(samples)

	print(type(samples))
	return np.sqrt(np.mean(samples**2))


def fft( samples=[], samplingFrequency = 100, duration=10, outputNumber = 10 , img_path = '' ):

	samplingInterval    = 1 / samplingFrequency;
	t = np.arange( 0.0 , duration, samplingInterval);
	#Simulation
	if len(samples)==0 :
		signal1Frequency     = 4;
		signal2Frequency     = 7;
		amplitude1 = np.sin(2*np.pi*signal1Frequency*t)
		amplitude2 = np.sin(2*np.pi*signal2Frequency*t)
		samples = amplitude1 +  amplitude2



	fourierTransform = np.fft.fft(samples)/len(samples)
	fourierTransform = fourierTransform[range(int(len(samples)/2))]
	tpCount     = len(samples)
	values      = np.arange(int(tpCount/2))
	timePeriod  = tpCount/samplingFrequency

	frequencies = values/timePeriod
	intensities = abs(fourierTransform)

	################# create and save plot ###############
	figure, axis = plotter.subplots(2, 1)
	plotter.subplots_adjust(hspace=1)
	axis[0].set_title('input samples')
	axis[0].plot(t, samples)
	axis[0].set_xlabel('Time')
	axis[0].set_ylabel('Amplitude')
	axis[1].set_title('Fourier transform depicting the frequency components')
	axis[1].plot(frequencies, abs(fourierTransform))
	axis[1].set_xlabel('Frequency')
	axis[1].set_ylabel('Amplitude')
	#plotter.show()
	plotter.savefig(img_path + 'fft.png')
	plotter.close(figure)

	#####################################################

	idx   = np.argsort(-intensities)
	frequencies = np.array(frequencies)[idx]
	intensities = np.array(intensities)[idx]
	res = dict(zip(frequencies[0:outputNumber], intensities[0:outputNumber]))

	return res



