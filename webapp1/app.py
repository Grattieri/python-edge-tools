#!/usr/bin/env python3
########################################################################
# Demo code : WEBAPP1
########################################################################
import time, datetime, json, threading, os, sys
from flask import Flask
from flask import send_from_directory
import paho.mqtt.client as mqtt
import lib
########################################################################

broker_address="localhost"
dir_path = os.path.dirname(os.path.realpath(__file__))
web_path = os.path.dirname(os.path.realpath(__file__))+'/WEBROOT/'
print("Current application path :" + dir_path )
print("Current webroot path :" + web_path )

time0 = time.time()
time.sleep(1)

################# DB #########################

db_file = (dir_path + '/db.json')

try :
	with open(db_file , 'r') as reader:
		data = json.loads(reader.read())

except:
	data = lib.init_json_data()
	with open(db_file, 'w') as writer:
		writer.write(json.dumps(data))

time.sleep(1)

################# MQTT ########################
def on_message(client, userdata, message):
	topic = str(message.topic)
	payload = str(message.payload.decode("utf-8"))
	data[topic] = payload
	if "edge/data/count" == topic : update('counter', 1)

client = mqtt.Client("P1")
client.on_message=on_message
print("connecting to broker")
client.connect(broker_address)
client.loop_start()
client.subscribe("edge/data/#")
################ SERVLET ########################
app = Flask(__name__  )

@app.route('/<path:path>')
def static_file(path):
	return send_from_directory(web_path,path)

@app.route("/data")
def data_res():
	def _round(v):
		return round(v,2) if isinstance(v, float) else v

	return (json.dumps({k:_round(v) for k, v in data.items() } ))

@app.route("/")
def home():
	return send_from_directory(web_path,'index.html')

t1 = threading.Thread(target=app.run , args=('0.0.0.0', 5000, None, True )).start()

##################################################

def update(name, value) :
	global data
	timestamp = time.time()
	now  = datetime.datetime.fromtimestamp(timestamp)
	now0 = datetime.datetime.fromtimestamp(data[name+'_timestamp'])
	data[name+'_timestamp'] = timestamp
	data[name+'_ever'] += value
	data[name+'_year'] += value
	data[name+'_month'] += value
	data[name+'_hour'] += value

	if now.year != now0.year: data[name+'_year'] = 0
	if now.month != now0.month: data[name+'_year'] = 0
	if now.day != now0.day: data[name+'_day'] = 0
	if now.hour != now0.hour: data[name+'_hour'] = 0

	with open(db_file, 'w') as writer:
		writer.write(json.dumps(data))

	client.publish("edge/informations", json.dumps(data))
	time.sleep(1)

while 1 :

	nowTime = time.time()
	deltaTime = nowTime - time0
	time0 = nowTime

	if data.get('edge/data/operative','false') =='true':
		update( 'operative',  deltaTime / 60.0 )

	elif data.get('edge/data/maintenance','false') =='true':
		update( 'maintenance',  deltaTime / 60.0 )

	else:
		update( 'fault',  deltaTime / 60.0 )

	time.sleep(1)
